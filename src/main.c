/**
 ******************************************************************************
 * @file    GPIO/GPIO_IOToggle/Src/main.c
 * @author  MCD Application Team
 * @version V1.0.2
 * @date    30-December-2016
 * @brief   This example describes how to configure and use GPIOs through
 *          the STM32F7xx HAL API.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "arm_math.h"
#include "arm_const_structs.h"
#include "string.h"

/** @addtogroup STM32F7xx_HAL_Examples
 * @{
 */

/** @addtogroup GPIO_IOToggle
 * @{
 */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static GPIO_InitTypeDef GPIO_InitStruct;

/* Private function prototypes -----------------------------------------------*/
static void SystemClock_Config(void);
static void CPU_CACHE_Enable(void);

/* Private functions ---------------------------------------------------------*/
RNG_HandleTypeDef RngHandle;

/* Used for storing 8 Random 32bit Numbers */
uint32_t aRandom32bit[8];

#define FFT_SIZE 2048
//#define FFT_SIZE 32
#define SAMPLE_FREQ 8000.0
/* -------------------------------------------------------------------
 * External Input and Output buffer Declarations for FFT Bin Example
 * ------------------------------------------------------------------- */
float32_t hann_window[FFT_SIZE];
float32_t testInput[FFT_SIZE];
float32_t testSpectrum[FFT_SIZE];
// float32_t testOutput[FFT_SIZE];

/* ------------------------------------------------------------------
 * Global variables for FFT Bin Example
 * ------------------------------------------------------------------- */
//uint32_t fftSize = 1024;
//uint32_t ifftFlag = 0;
//uint32_t doBitReverse = 1;
/* Reference index at which max energy of bin ocuurs */

/* ----------------------------------------------------------------------
 Test Input signal contains 10KHz signal + Uniformly distributed white noise
 ** ------------------------------------------------------------------- */

arm_rfft_fast_instance_f32 S;
UART_HandleTypeDef UartHandle;
/* Private function prototypes -----------------------------------------------*/
#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
 set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */

/* ----------------------------------------------------------------------
 * Max magnitude FFT Bin test
 * ------------------------------------------------------------------- */
static void Error_Handler(void) {
	/* User may add here some code to deal with a potential error */

	/* In case of error, LED3 is toggling at a frequency of 1Hz */
	while (1) {
		/* Toggle LED3 */
		BSP_LED_Toggle(LED3);
		HAL_Delay(500);
	}
}

void compute_hann_window(void) {
	int index = 0;
	while (index < FFT_SIZE) {
		hann_window[index] = 0.5 * (1 - cos(2 * PI * (index + 0.5) / FFT_SIZE));
		index++;
	}
}

float32_t uniform(float32_t min, float32_t max) {
	uint32_t aRandom32bit;
	if (HAL_RNG_GenerateRandomNumber(&RngHandle, &aRandom32bit) != HAL_OK) {
		/* Random number generation error */
		Error_Handler();
	}
	return min + (max - min) * (uint32_t) aRandom32bit / 4294967295.0;
}

float32_t df4(float32_t left, float32_t center, float32_t right) {
	float32_t q;
	q = 0.666666666666666666666666667 * (right - left) / center;
	return 2.0 * (sqrt(1.0 + q * q) - 1.0) / q;
}

float32_t get_mag(float32_t *cspectrum, int index) {
	index = index << 1;
	float32_t real = cspectrum[index];
	index++;
	float32_t imag = cspectrum[index];
	return sqrt(real * real + imag * imag);
}

float32_t compute_error(float32_t amp, float32_t fr, float32_t sf,
		float32_t phase, float32_t noise) {
	int index = 0;
	float32_t st = 1 / sf;
	float32_t two_pi_fr_st = 2 * PI * fr * st;
	float32_t df = sf / FFT_SIZE;
	// printf("** COMPUTING testInput ** \n\r");
	while (index < FFT_SIZE) {
		testInput[index] = amp * hann_window[index]
				* sin(index * two_pi_fr_st + phase)
				+ uniform(0.0 - noise, noise);
		// printf("%d %f\n", index, testInput[index]);
		index++;
	}
	// printf("** COMPUTING RFFT ** \n\r");
	/* Compute the RFFT */
	arm_rfft_fast_f32(&S, testInput, testSpectrum, 0);
	//	index = 0;
	//	while (index < FFT_SIZE) {
	//		printf("Complex Spectrum[%d]=%e\r\n", index, testSpectrum[index]);
	//		index++;
	//	}
	/* Process the data through the Complex Magnitude Module for
	 calculating the magnitude at each bin */
	// arm_cmplx_mag_f32(, testOutput, FFT_SIZE);
	/* Calculates maxValue and returns corresponding BIN value */
	// arm_max_f32(testOutput, fftSize, &maxValue, &testIndex);
	int line = (int) (fr / df);
	if (get_mag(testSpectrum, line + 1) > get_mag(testSpectrum, line)) {
		line++;
	} else {
		if (get_mag(testSpectrum, line - 1) > get_mag(testSpectrum, line)) {
			line--;
		}
	}
	//	printf("line--: %e\r\n", get_mag(testSpectrum, line - 2));
	//	printf("line- : %e\r\n", get_mag(testSpectrum, line - 1));
	//	printf("line  : %e\r\n", get_mag(testSpectrum, line));
	//	printf("line+ : %e\r\n", get_mag(testSpectrum, line + 1));
	//	printf("line++: %e\r\n", get_mag(testSpectrum, line + 2));
	float32_t efr = df
			* (line
					+ df4(get_mag(testSpectrum, line - 1),
							get_mag(testSpectrum, line),
							get_mag(testSpectrum, line + 1)));
	float32_t error = efr - fr;
	// printf("error: %e\r\n", error);
	return error;
}

void test_fft(int stat_n, float32_t sf) {
	int index = 0;
	float32_t randomline = FFT_SIZE / 8; // TBD + 0.5;
	float32_t df = sf / FFT_SIZE;
	float32_t error;
	float32_t error_max = 0.0;
	float32_t error_sum = 0.0;
	float32_t phase;
	float32_t fr = randomline * df;
	float32_t cent = (fr * pow(2.0, 1.0 / 1200.0)) - fr;
	while (index < stat_n) {
		// randomline = random.uniform(myline, myline + 1.0)
		phase = uniform(0.0, 2 * PI);
		error = compute_error(1.0, fr, sf, phase, 0.1);
		// printf("error: %.3f[cent]\n", error / cent);
		// cent = (fr * pow(2.0, 1.0 / 1200.0)) - fr;
		// print "line: %.3f, " % randomline,
		// print "phase: %.3f, " % phase,
		// print "error: %.3f cent" % (error / cent)
		error_sum += error;
		if (fabs(error) > fabs(error_max)) {
			error_max = error;
		}
		index++;
	}
	printf("maxerror: %.3f[cent] ", error_max / cent);
	printf("average_error: %.3f[cent]\r\n", (error_sum / stat_n) / cent);
}
void print_randoms(void){
	int index = 0;
	while(index < 20){
		printf("random[%d]=%f\r\n", index, uniform(0.0, 1.0));
		index++;
	}
}

/**
 * @brief  Main program
 * @param  None
 * @retval None
 */
int main(void) {
	/* This sample code shows how to use GPIO HAL API to toggle GPIOB-GPIO_PIN_0 IO
	 in an infinite loop. It is possible to connect a LED between GPIOB-GPIO_PIN_0
	 output and ground via a 330ohm resistor to see this external LED blink.
	 Otherwise an oscilloscope can be used to see the output GPIO signal */

	/* Enable the CPU Cache */
	CPU_CACHE_Enable();

	/* STM32F7xx HAL library initialization:
	 - Configure the Flash prefetch
	 - Systick timer is configured by default as source of time base, but user
	 can eventually implement his proper time base source (a general purpose
	 timer for example or other time source), keeping in mind that Time base
	 duration should be kept 1ms since PPP_TIMEOUT_VALUEs are defined and
	 handled in milliseconds basis.
	 - Set NVIC Group Priority to 4
	 - Low Level Initialization
	 */
	HAL_Init();

	/* Configure the system clock to 216 MHz */
	SystemClock_Config();

	/* -1- Enable GPIO Clock (to be able to program the configuration registers) */
	__HAL_RCC_GPIOB_CLK_ENABLE()
	;
	__HAL_RCC_USART3_CONFIG(RCC_USART3CLKSOURCE_PCLK1);
	__HAL_RCC_USART3_CLK_ENABLE()
	;
	__HAL_RCC_GPIOD_CLK_ENABLE()
	;
	__HAL_RCC_USART3_FORCE_RESET();
	__HAL_RCC_USART3_RELEASE_RESET();
	__HAL_RCC_RNG_CLK_ENABLE()
	;
	/* -2- Configure IO in output push-pull mode to drive external LEDs */
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;

	GPIO_InitStruct.Pin = GPIO_PIN_0;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*##-1- Configure the UART peripheral ######################################*/
	/* Put the USART peripheral in the Asynchronous mode (UART Mode) */
	/* UART configured as follows:
	 - Word Length = 8 Bits (7 data bit + 1 parity bit) :
	 BE CAREFUL : Program 7 data bits + 1 parity bit in PC HyperTerminal
	 - Stop Bit    = One Stop bit
	 - Parity      = ODD parity
	 - BaudRate    = 9600 baud
	 - Hardware flow control disabled (RTS and CTS signals) */
	UartHandle.Instance = USARTx;

	UartHandle.Init.BaudRate = 115200;
	UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
	UartHandle.Init.StopBits = UART_STOPBITS_1;
	UartHandle.Init.Parity = UART_PARITY_NONE;
	UartHandle.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	UartHandle.Init.Mode = UART_MODE_TX_RX;
	UartHandle.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(&UartHandle) != HAL_OK) {
		/* Initialization Error */
		Error_Handler();
	}

	/* Output a message on Hyperterminal using printf function */
	printf("\n\r** FFT example. **\n\r");

	/*## Configure the RNG peripheral #######################################*/
	RngHandle.Instance = RNG;
	printf("** HAL_RNG_DeInit **\n\r");

	/* DeInitialize the RNG peripheral */
	if (HAL_RNG_DeInit(&RngHandle) != HAL_OK) {
		/* DeInitialization Error */
		Error_Handler();
	}
	printf("** HAL_RNG_Init **\n\r");
	/* Initialize the RNG peripheral */
	if (HAL_RNG_Init(&RngHandle) != HAL_OK) {
		/* Initialization Error */
		Error_Handler();
	}
	printf("** arm_rfft_fast_init_f32 **\n\r");
	arm_rfft_fast_init_f32(&S, FFT_SIZE);
	printf("** compute_hann_window **\n\r");
	compute_hann_window();
	printf("** print some random numbers **\n\r");
	print_randoms();
	printf("** start the loop **\n\r");

	/* -3- Toggle IO in an infinite loop */
	while (1) {
		// printf("** HAL_GPIO_TogglePin ** \n\r");
		HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_0);
		/* Insert delay 100 ms */
		// HAL_Delay(100);
		test_fft(100, SAMPLE_FREQ);
	}
}

/**
 * @brief  System Clock Configuration
 *         The system Clock is configured as follow :
 *            System Clock source            = PLL (HSE)
 *            SYSCLK(Hz)                     = 216000000
 *            HCLK(Hz)                       = 216000000
 *            AHB Prescaler                  = 1
 *            APB1 Prescaler                 = 4
 *            APB2 Prescaler                 = 2
 *            HSE Frequency(Hz)              = 8000000
 *            PLL_M                          = 8
 *            PLL_N                          = 432
 *            PLL_P                          = 2
 *            PLL_Q                          = 9
 *            PLL_R                          = 7
 *            VDD(V)                         = 3.3
 *            Main regulator output voltage  = Scale1 mode
 *            Flash Latency(WS)              = 7
 * @param  None
 * @retval None
 */
static void SystemClock_Config(void) {
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_OscInitTypeDef RCC_OscInitStruct;

	/* Enable HSE Oscillator and activate PLL with HSE as source */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
	RCC_OscInitStruct.HSIState = RCC_HSI_OFF;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 8;
	RCC_OscInitStruct.PLL.PLLN = 432;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 9;
	RCC_OscInitStruct.PLL.PLLR = 7;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		while (1) {
		};
	}

	/* Activate the OverDrive to reach the 216 Mhz Frequency */
	if (HAL_PWREx_EnableOverDrive() != HAL_OK) {
		while (1) {
		};
	}

	/* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
	 clocks dividers */
	RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK) {
		while (1) {
		};
	}
}

/**
 * @brief  CPU L1-Cache enable.
 * @param  None
 * @retval None
 */
static void CPU_CACHE_Enable(void) {
	/* Enable I-Cache */
	SCB_EnableICache();

	/* Enable D-Cache */
	SCB_EnableDCache();
}

/**
 * @brief  Retargets the C library printf function to the USART.
 * @param  None
 * @retval None
 */
PUTCHAR_PROTOTYPE {
	/* Place your implementation of fputc here */
	/* e.g. write a character to the USART3 and Loop until the end of transmission */
	HAL_UART_Transmit(&UartHandle, (uint8_t *) &ch, 1, 0xFFFF);

	return ch;
}
#ifdef  USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{
	/* User can add his own implementation to report the file name and line number,
	 ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while (1)
	{
	}
}
#endif

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
